= Produktplan för 2025

Fokus fram till den 2025-02-28 är att skapa en produktplan vi själva tror på för året.

Data som kan delas ska göras tillgänglig på enklast möjliga sätt: *Förenkla leveranser som data i fil som följer öppen standard för att underlätta användande.*

== Uppgift 1 - Följande mening kommer användas för att beskriva taxonomin om ni ej återkopplar

Återkoppla Jonas: Senast 2025-02-09

Klassificera, standardisera och distribuera yrkes- och kompetensbegrepp för svensk arbetsmarknad

== Underlag för taxonomiinförandet (med Lisa)
Johan: Ta fram de filer och data som vi fått in från de olika grupperna som vi skickade ut enkäter till.

== Uppgift 2 - Yrken & kompetenser som masterdata

Återkoppla Jonas: Senast 2025-02-13

Nuvarande version 1.1:
https://space.arbetsformedlingen.se/sites/enhetenjobtech/_layouts/15/WopiFrame.aspx?sourcedoc=%7B45007AE5-15E3-47DB-BE66-22E3693D12BE%7D&file=20250204%20%C3%96ppna%20data%20Produktplan%201.1%20.pptx&action=default&IsList=1&ListId=%7BF39F7281-B9B2-456A-A7DA-65F283D95CC8%7D&ListItemId=3628

OBS! GYR-Y ingår i de leveranser vi enas om i början. Men någon gång kommer vi behöva rapportera separat för tid för GYR-Y. Detta projekt kan komma att finansieras med immateriella medel.

=== Frågor

Vem skriver vi för när det gäller leveranserna? Beskrivande av vad vi tänkt göra; inte säljande. Men så leveranserna blir sånt vi kommer göra i år.

Hur beskriver vi leveranser som något avgränsat i tid? Inte bara löpande leveranser och arbete?

=== Leverans 1 - Kategorisering: Yrken och kompetenser i taxonomi (Redaktion)
- 3725 (25 \* 149) yrkesbenämningar
- Översyn av yrkesbenämningar i samtliga yrkesgrupper i SSYK nivå 4, inklusive beskrivningar och viktiga kompetenser för respektive yrkesbenämning.

=== Leverans 2 - Standardiseringsarbete; som exempelvis uppdatering av SUN-koder

- SSYK versioner
- SUN versioner
- ESCO versioner
- RDF/SKOS
- https://standard.publiccode.net

=== Leverans 3 - Implementeringsstöd vid myndighetens användning av taxonomin, yrken och kompetenser

- Sökruta 


== Bonusuppgift 3 - Milstolpar

För oss själva är det bra att fundera igenom vad betydande att ha a) gjort betydande prestationer eller b) nått ett kritiskt skeende i utvecklingen. Ej avgörande för planen. Mer viktigt för oss själva.
