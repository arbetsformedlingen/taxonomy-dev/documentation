= 8 Week Plan no. 3
:previous: 2024-03-05.ad
:issues: https://gitlab.com/groups/arbetsformedlingen/taxonomy-dev/-/issues/?sort=updated_desc&state=opened&first_page_size=100
:board: https://gitlab.com/groups/arbetsformedlingen/-/boards/7294572?label_name[]=Team%20Taxonomy-dev

link:{previous}[Previous plan]

== Where are we now?

* **Taxonomy Adoption Project (TAP) has started**
** Wait and see what Marika wants help with
* Adding documentation
** Finish the assigned tasks
** Integrate the documentation written by David N. and redaktionen
** Ask Chark to review the documentation
* Making CI/CD work well enough to be a helpful thing and not something we fight with
** Adding tests to be sufficiently secure in auto-deploying
*** Unit tests
*** Integration tests
*** Remote (running system) service tests
** Looking at deployment environments / pipeline
** Fixing the Swagger endpoint so we keep what we promise (request parameters are validated and responses are defined)
* Work on Cache
* Work on the CI pipelines for all other non-archived projects
* Moving away from Datomic Cloud
** We can get a zipped version of the datomic database
** We have a way of writing to a local disk
* Talking about adopting Standard for Public Code
* Talking about implementing RDF IRI:s for identifiers

== What should we work on for the next two months?

* Cache system
* Leaving Datomic Cloud
** Backups
** Test if the editor works with a local write DB
* Tests
** Update the recorded data so we include the mappings endpoint
** Finalise suggested tests
** Speed up the tests
** Add test timing to the CI/CD pipeline
* CI/CD
** Update the https://gitlab.com/groups/arbetsformedlingen/taxonomy-dev/-/epics/6 with a plan for this
** Finalise a automated deployment pipeline (AutoDevOps?)
*** Set of conditions to deploy to test
*** Set of conditions to deploy to production
** Run tests for a merge request against a pod with the branch built image
*** The system is too slow to start in time for the tests
*** Schema thesis tests
**** Make the API endpoints confirm to the spec
***** Look into generating the code from an OpenAPI spec
* Update Dockerfiles
** Make sure the Dockerfiles are up to date
** Make sure the Dockerfiles are as small as possible
* Upgrades
** Reitit 0.7.0
*** Merging spec fails, better support for Malli
**** Create an issue describing problems and where more information can be found
**** Figure out how to solve this
**** Possibly look at an automatic rewrite of spec to Malli
** Datahike update
*** Await merge of Jonas Ö. PR:s
*** Run the tests with Datahike as backend and see what fails
**** Identify differences between Datomic and Datahike
**** Figure out resolutions
*** See if the multi backend runner needs to be updated
* Documentation
** Possibly split the documentation into its own pod and/or update cache rules for it
** Finish the assigned tasks
** Integrate the documentation written by David N. and redaktionen
** Ask Chark to review the documentation
* Update Atlas to show Green Concepts (Redaktionen)

---

* Topics to consider
** Work with Redaktionen
** Concept recognition
*** Code review/translation to Clojure
** Term collection database (Redaktionen)