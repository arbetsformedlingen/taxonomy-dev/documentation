## Siggesta förmiddagsdiskussion 2022-05-04: Mål och nytta

Vi diskuterade punkter kring JobTechs mål och syfte och berörde också regeringsuppdraget. [Sammanfattning](sammanfattning) i slutet av dokumentet.

### Vad som sades

**Anton**: Går vi åt rätt håll?

**Fredrik**: Om man vill kolla på målen för KLL, då finns det riksdagsbundna mål. Det är riksdagen som beslutar det. Problemet för vårt område är att den är tre till fem olika departement. Inom arbetsmarknadsmarknadsområdet trillar det ner till att "få arbetsgivare och tagare att hitta varandra". Summa summarum: det finns inte en röst. Om man kollar till KLL. Hur konkret är det? Om vi pratar mättal, vad är det? Jag tycker att mellanmålen är bättre. Det är det konkreta vi är ute efter.

**Anna**: Bortsett från det, innan KLL har alla team gjort olika saker. Vad är anledningen till att vi gjorde dessa saker? Var det någon som talade om att vi skulle göra dessa saker, eller var det egna initiativ. Hur kom de till?

**Lisa**: Man vill få fram den mest effektiva matchningsinfrastrukturen i världen. Det kallades "digital matchning". Det var vårt uppdrag initialet.

**David**: Vårt uppdraget var att förbättra arbetsmarknadens funktionssätt. Där gjorde jag kopplingen att det var bredare än vad arbetsförmedlingen traditionellt gör. Mer kopplat till arbetsmarknadens funktionssätt och digital matchning.

**Anna**: Med tanke på hur viktigt digitaliseringen är tror jag att vi gör rätt saker.

**Tamaz**: Om vi lyssnar på Linda verkar vi göra rätt saker.

**Lisa**: Skulle vi göra en story, t.ex. om individen som fick ett jobb? Hur skulle den storyn se ut enligt vårt perspektiv? Då får vi in alla våra områden. Men det är inte så mätbart.

**Tamaz**: är medborgaren vår primära kund? Våra primära kunder är några sorters organisationer som kan utnyttja vår infrastrukturer. Det kan man mäta, t.ex. hur många organisationer som använder API:er.

**David**: Men vad ger det för nytta? Minns Eric Ries bok "The lean startup" och "Vanity metrics". Vi kan räkna "antalet regeringsuppdrag". Då kommer vi in i det här, känner vi igen det som beskrivs.

**Tamaz**: Man måste ha *rätt* KPI:er. Vad blir effekten av dem som nyttjar våra tjänster. Man har ju google analytics, kanske vi behöver behöver ett verktyg för att mäta hur de här tjänsterna nyttjas.

**Anna**: matomo

**David**: Vi ville bli plattformsledande. Johan Linåker, hur mäter man hälsan i ett öppet system. Ett antal KPI:er, t.ex. Mad Professor reports. Vi har lite grann tappat eftersom vi har regeringsuppdragen som byter fokus, så det blir svårare.

**Fredrik**: Jag har hört på regeringskansliet att omvärlden har förändrats och att det ledde till regeringsuppdraget. Den omställningen har lett fram till regeringsuppdraget. Om vi går från KPI:er som vi kan mäta konkreta, vad blir effekten av det?

**Mattias**: Hur jobbar vi på ett strukturerat sätt för att få en gemensam problembild? Vi kan inte mäta hur bra det gick om vi inte har ett problem. För mig finns det inte att jobba med tekniska lösningar om det inte finns ett problem.

**Lisa**: Den digital infrastrukturen ser olika ut beroende på om det är matchning eller skicka ut statistik.

**Fredrik**: För att förstå problemen måste vi gå igenom målbilden. Vad är det för olika verksamhetsområden som vi pratar om och som verksamheten ska serva.

**Lisa**: Ska vi bygga digital infrastruktur för ett område eller alla områden.

**Fredrik**: Om den inte är sammanhållen kan vi bygga en för arbetsmarknaden och en för skolvärlden. Vi bygger för alla områden.

**Mattias**: Jobbar vi med rätt saker? Det kan vi inte svara på om vi inte vet vad vi ska lösa.

**Tamaz**: Jag har en fundering: Vi pratar inte alltid om en teknisk deployad lösning, men det kan också vara processer, riktlinjer och dokumentation, som jag förstår i begreppet "datainfrastrukturen". Det är inte bara den tekniska?

**Anna**: Det är viktigt att vi har en gemensam definition. Om vi inte ens vet det...

**Tamaz**: Delar i regeringsuppdraget är redan befintliga tjänster, t.ex. taxonomin. Regeringsuppdragets datainfrastruktur är inte bara Arbetsförmedlingen: Vi ska ta fram en lösning, men den är inte bara teknisk, utan även regelverk. Den ska vara till gagn för hela Sverige.

**Anna**: Vad kommer vi fram till?

**Mattias**: Om vi inte vet problemet, vet vi inte att vi jobbar med rätt sak. Problemet med ord som "matchning" är så fluffigt, digital infrastruktur, etc. Vi måste **definiera** vad orden betyder inom JobTech.

**David**: Vi jobbar långt fram. Korta inlärningsloopar och snabbt kunna förkasta det som inte fungerar. Jag upplever att vi jobbar med rätt saker.

**Anton**: Från mitt perspektiv. När jag började på JobTech var det tydligare vad problembilden var, då fokuserade vi på att öppna upp Arbetsförmedlingens data och göra det tillgängligt. T.ex. att vi tog fram Connect Once. På senare tid har målbilden skiftats, med större utmaningar. Det är inte bara Arbetsförmedlingen, utan hela Sverige. Har vi den hela problembilden? Det kan man fråga sig. Vi har fått uppdraget att ta fram det här. Det här med gemensam begreppsstruktur och digital infrastruktur. Min bild är att den ska vara generell, fungera för många olika områden och strukturer. Man ska kunna dela data, men vilka data ska det vara. Hos oss kan man ta fram ett gemensamt språk, och det kan användas inom den här strukturen.

**Tamaz**: Om taxonomin ingår i datainfrastrukturen, då måste vi prata mer med varandra och samarbeta. Vi måste se till att de här två lösningarna är kompatibla.

**David**: Vi kanske behöver ett problem innan vi börjar koppla ihop dem.

**Mattias**: Varför ska vi snacka om olika system. Istället bör vi fråga oss, vad är grundproblemet? Jämföra med sjukvården: Man har blandat till en medicinsk cocktail och väntar på rätt patient.

**Tamaz**: Liknelsen med röntgenbilder och patientjournaler. Då bygger man en maskin som tar röntgenbilder, men inte har stöd för att lägga in dem i journalerna.

**Fredrik**: Det finns många problem. Vilket är det vi ska ta itu med? "Jobbar vi med rätt mål" -> "hur tar vi fram ett strukturerat arbetssätt för att ta fram mål". På vilken tidshorisont? Vi funderade i höstas på ett forum internt där vi inte pratar på vad vi gjort, men också "vad ser vi för beroenden med varandra"? Och belysa dessa beroenden. Det kan bli intressekonflikter. Jag spinner gärna vidare på lösningar, hur kan vi prata med varandra.

Det är olika funktionalitet som datainfrastrukturen måste kunna (4G): Gather, Guard, Grow, Give.
Vilka datamängder behöver vi hantera tillsammans med andra?
Det finns politiskt specifikt och förvaltningsspecifikt. Men finns det något *domän*specifikt.
Än så länge är vi fortfarande på "vi ska ha en gemensam datainfrastruktur". Vi behöver bli mer konkreta. Vad är det för dimensioner vi behöver placera in oss i för att kunna prata om vilka beroenden vi har med andra myndigheter.

**Lisa**: Det är en mismatch på arbetsmarknaden. Om vi utgår från individperspektiv behöver vi närma oss problemet från ett annat håll. Bygger vi personcentrerat eller från något annat håll? Bygger vi infrastruktur för personcentrerat eller det mer traditionella synsättet inom AF?

**Tamaz**: Du nämnde förvaltning. Vi har ingen förvaltningsorganisation. Vem ska förvalta den sammanhållna datainfrastrukturen? Är det IT som ska ta fram den?

**Fredrik**: Det ska samordnas med de andra myndigheterna.

**Tamaz**: Då måste man tala med de andra myndigheterna tidigt.

**Anna**: Det här med mäta. Matomo det är som Google Analytics, vi köper det verktyget. Men professorrapporten, vad innehåller den?

**Calamari**: Vi samlar inte data från olika system och presenterar det. Hur mycket systemen används. Vi har diskuterat länge hur våra KPI-värden ska se ut.

**David**: Finns det behov av synk, finns det ett uppdrag idag.

**Tamaz**: Jag har inte förstått riktigt att de olika delarna jobbar med samma sak. Om man jobbar med samma sak, behöver man stämma av. Så att man inte får lösningar som är svåra att harmoniera.

**Nils**: Jag har tanke med vad vi ska mäta. "Att stärka individen" eller "att stärka företagen". För mig är det individen vi vill stärka och utbilda. Jag vill inte bara hjälpa Northvolt att få en massa arbetare.

**David**: Ska vi lägga in en punkt "individcentrerat" på ostindiska.

**Nils**: Med egna data har man mer att säga till om. Förr i tiden lät man Telia köpa upp en infrastruktur för att den skulle komma alla till del.

**Lisa**: Det skulle vara kul att kolla på Carls hållbarhet i offentligt lärande.

**David**: Också intressant att utforksa personcentrering i SOLID-casen som vi tar fram.

### Sammanfattning

* Att det är svårt att formulera mer konkreta mål för regeringsuppdraget kan ha att göra med att tre till fem olika departement. Det är alltså **inte en röst**.
* Vi levererar **tjänster åt andra organisationer**, hur kan vi **mäta nyttan/effekten** av dessa tjänster gör på arbetsmarknaden?
* För att ha något meningsfullt att mäta behöver vi en **problembild**. Den tycks inte vara tydlig i dagsläget.
* Berörde [**"vanity metrics"**, Eric Ries](https://hbr.org/2010/02/entrepreneurs-beware-of-vanity-metrics). Mått som inte är meningsfulla. Att räkna API-anrop kan vara meningsfullt, men det förmodligen inget bra mått på hur väl vi utför vårt uppdrag.
* Vi nämnde **Mad Professor Reports**, som redovisar olika mått på hur våra IT-system fungerar.
* Vi berörde **4G: Gather, Guard, Grow, Give**, apropå den digitala infrastrukturen.
* Med *digital infrastruktur* menar vi inte nödvändigtvis bara en teknisk lösning, det kan också vara **processer, riktlinjer och dokumentation**.
* Vi nämnde i korthet **Matomo** och **Google Analytics**.
* Vi kanske bör **utforska personcentrering i SOLID-casen** som vi tar fram.
* *Fortsatt arbete:* **Hur kommer vi vidare?** Det verkar fortfarande oklart hur vi kan mäta vår samhällsnytta.
