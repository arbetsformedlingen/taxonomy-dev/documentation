## Siggesta eftermiddagsdiskussion 2022-05-04: Extern samverkan

Här diskuterade vi samspelet mellan JobTech och andra aktörer:

Lappar för att initiera diskussionen:

* Stakeholder requirments?
* Vad är problemen inom livslångt lärande och hur kan vi arbeta på ett strukturerat sätt för att få en gemensam problembild. Nu har vi lösningsförslag på odefinierade problem.
* Hur kan komms teamet bistå andra teamen? Förväntningar?
* Näromvärldanalys (kartan över sammanhang vi är involverade i).

There is a [summary](summary) at the end of this document.

### Vad som sades

**Fredrik**: What initiated the discussion was a discussion yesterday about unit involvements with other stakeholders. You have to know about the environment where you work: External analysis. Knowledge about the external world from our perspective. Our focus was what we are doing together with other partners, and our common goals. Instead of looking at "where is the horizon for Sweden in 2025", we want to know the horizon *with* our external partners.

**Tamaz**: How to harmonize the view we have on our tasks that we have.

**David**: The second note is from the communications team and Adelina: How can we assist the other teams in JobTech with communication? And the other note is from Mattias: How we can gain a common understanding about the problems in lifelong learning. Let's start the discussion about the communication team and analysis with external partners.

**Tamaz**: Is it a topdown kind of process, where the leadership decides the problems or should we discuss it together? E.g. the definitions in KLL, what does "sammanhållen infrastruktur" mean?

**Fredrik**: We don't want to work in an environment where the instructions come from the leadership.

**Adelina**: Are we missing a vision, or do we have a vision?

**Fredrik**: We have an abstract vision. We need to find processes for understanding more together how our close neighborhood is working. That is, taking down the grand vision to something that we concretely work on now.

**Adelina**: Do you feel that you are missing information about ongoing projects in other teams? Is it a practical solution or an abstract vision?

**Tamaz**: I was not aware that other teams were working on similar projects as I was working on. Build a picture where we can see what each team is working on. It does not have to be a written document, rather a picture or some kind of map.

**Anna**: Do you think you get enough information on the coordination circle?

**Tamaz**: I have not participated much.

**David**: It is difficult with knowledge sharing, what part each person is working on. To know enough to ask questions. Maybe **a map is a good idea**. Maybe a common roadmap. I don't know how it is supposed to look. Who is drawing it, how it is drawn, who is keeping it up-to-date.

**Tamaz**: I don't have the big picture and I am not the only one. We could have a team session to produce this map.

**Adelina**: There should be something that is visual that will show the technical aspects of each team. There should be something that is (i) visual and (ii) technical.

**Tamaz**: It needs to show *who is doing what* in this bigger infrastructure.

**Anna**: Maybe we could have some demo day for internal JobTech. People can come with suggestions about what they want to know more about. To understand the bigger picture more.

**Adelina**: It goes in line with a session where people share knowledge. But questions arise: Am I knowledgable enough to make a demo for one hour. But it could be very broad. It seems it would be interesting and beneficial for everyone in the unit.

**Fredrik**: In the coordination circle, we have the teams describing what they have been going through last week and we have leadership team giving information. But maybe the leadership team could give more information. At least I know from Skolverket that they think we are walking along the waterfall model, but we don't. How do we communicate with them so that they understand us.

**David**: Maybe it is a challenge for us to communicate among us.

**Adelina**: What would the most proper way for me to take this information? Is it a demo? Is it a presentation? What method?

**Fredrik**: I am going to present the semantikprojektet. I don't know how many of you who know and I don't know if you are interested.

**Adelina**: I would encourage you to follow us on LinkedIn and Twitter if you have.

Maybe a knowledge session would be useful.

**Anna**: And maybe evaluate the demoweek afterwards to see if it was useful.

**David**: If we look just outside of JobTech to better understand.

**Adelina**: I don't want to interrupt the teams and ask about teams about, say, "cybersecurity", where I have zero knowledge.

**Anna**: Also in order to tell external people what JobTech is, we have to start inside and understand what JobTech is.

**Erik**: We have started something called "fika stund" within sekretariat for KLL. To get to know each other, to make it easier to collaborate. It is more informal. But you can do a demo. But you don't have to discuss the government assignment. *Some more was said about the additional difficulties of running remote coffee breaks but they can help improve the collaboration between people from different teams*.

**Tamaz**: Are you in the communications team doing external communication, or are you also doing internal communication? Regarding documentation, e.g. in Gitlab? Not all developers are good at documenting.

**Lona**: Why are there closed repos on Gitlab?

**David**: I think we should open up everything that can be opened.

**Fredrik**: Are there highlevel descriptions of what is in the Gitlab repos?

**Tamaz**: There is a README file for each repo. While you are developing, it is useless to maintain a readme. But when you are releasing, it is meaningful to have a readme.

### Summary

* Have a **knowledge session** to understand what everyone is working on.
* Create a **map** of what everyone is working on.
* **Evaluate the demo week** afterwards to see if it was useful.
* Strive to keep as many of our **code repos open** as possible.
* Strive to have **good readme-files** in the code repos.
* Some additional effort seems necessary to get people to participate in **remote coffee breaks** but they can nevertheless be a good platform to improve and facilitate collaboration between teams in an informal setting that is not strictly work-related.
