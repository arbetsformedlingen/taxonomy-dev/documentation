## Christmas and New Year break planning

Preference notation:

* `-`: Day off in any case. Treat it the same as a weekend.
* `0`: Does not care about day off
* `1`: Would like day off
* `2`: Strongly want day off
* `3`: Vacation reserved

| Day           | Comment        | Johan | Jonas Ö | Sara | Adam | Jonas C | Persons present     |
|---------------|----------------|-------|---------|------|------|---------|---------------------|
| Mo 2024-12-23 | 4 hours work   | 2     | 3       | 0    | 0    | 3       | Adam, Sara          |
| Tu 2024-12-24 | Off            | -     | -       | -    | -    | -       |                     |
| We 2024-12-25 | Off            | -     | -       | -    | -    | -       |                     |
| Th 2024-12-26 | Off            | -     | -       | -    | -    | -       |                     |
| Fr 2024-12-27 | Needs presence | 2     | 1       | 3    | 1    | ?       | Jonas Ö, (Jonas C?) |
|               |                |       |         |      |      |         |                     |
| Mo 2024-12-30 | Needs presence | 2     | 3       | 3    | 1    | 3       | Adam                |
| Tu 2024-12-31 | Off            | -     | -       | -    | -    | -       |                     |
| We 2025-01-01 | Off            | -     | -       | -    | -    | -       |                     |
| Th 2025-01-02 | Needs presence | 2     | 2       | 0    | 2    | 3       | Sara                |
| Fr 2025-01-03 | Needs presence | 2     | 2       | 0    | 2    | ?       | Sara, (Jonas C?)    |
|               |                |       |         |      |      |         |                     |
| Mo 2024-01-06 | Off            | -     | 3       | -    | -    | -       |                     |
