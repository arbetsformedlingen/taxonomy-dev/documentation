# Lifelong Learning Ideas

The purpose of this document is to record ideas and our understanding of the lifelong learning project.

## Meeting 2021-11-25

**Idea:**
What if we track supply/demand of labor market bucketed by skill+geographic location, establish mappings between occupations and educations, and then track supply/demand of educations bucketed by same occupations+locations, and then publish them and highlight discrepancies between supply/demand in different buckets?

Define the layers a solution should have; e.g. datalayer, enrichment layer, matching layer or other services that could be useful for many actors

**How do we make the distinction between digital infrastructure (which should enable a potential for the development of services) and services ? In the "Återkoppling"-dokument p. 40 it states that AFs role is to provide infrastructure, not services.** Further evidence that we should focus on infrastructure rather than coding of actual services:

Page 14: Även om distinktionen mellan infrastruktur och tjänst än så länge inte är glasklar i den här domänen så utgör skillnaden en skarp avgränsning för uppdraget. Målet med en sammanhållen digital infrastruktur är inte att den i sig ska tillhandahålla eller ta fram tjänster som är till nytta för individer och arbetsgivare. Målet med infrastrukturen är att stärka förutsättningarna att tillhandahålla och ta fram sådana tjänster.

Page 13: För informationsöverföring behövs någon form av identifiering och inloggning för olika användare, standarder för dataöverföring, samt säkerhetslösningar för att hindra obehöriga att ta del av datainnehållet. De tre delarna kan tänkas vara en kärna i den infrastruktur som eftersträvas.

**Idea:**
As joint effort between eight governmental agencies, with one aim being improving interoperability, it seems reasonable to talk to the seven other agencies, to agree on high priority tasks. (p. 10: "ett tydligt uppdrag till myndigheterna att gemensamt etablera formerna för en sammanhållen infrastruktur.")

**Idea:**
The creation of translation keys between "concept structures" (under the header "Utveckling av gemensamma begrepp"). This could for instance be the taxonomy-ESCO mapping. We could build tools to facilitate such translations on the long term.

**Idea:**
How do we establish a relation with developers of services? Also on p. 40, the instruction reads that it is the feedback from such developers that should be the guiding principle in the development of the infrastructure.

**Idea:**
CV-links

**Idea:**
Edu-links

**Idea:**
Develop 3-way-matching algorithms

**Idea:**
"Data som strategisk resurs för artificiell intelligens": Efforts to build datasets are very much in line with the lifelong learning project.

**Idea:**
From the TSL meeting, we learnt that it would be useful to provide AI support to predict trends of what skills will be in demand, e.g. electrification in the car industry.

**Idea:**
I like the idea of making data that we mine/analyze public in the form of JSON files at data.jobtechdev.se. It is very accessible to the public.

**Idea:**
Semantic annotation of education description seems to be in line with this project, for example "tjänster för matchning, vägledning"

**Idea:**
If we associate a semantic vector with each taxonomy concept (e.g. based on S-BERT), it could be easier to get more useful results in search queries. That, in turn, can give better results when searching for educations tagged with taxonomy concepts. This is in line with KLL.

### Discussion

**How do we make sure we develop according to the mission statement?**
- The återrapport [2] defines some bounds of what we are supposed to do: *"Målet med
en sammanhållen digital infrastruktur är inte att den i sig ska tillhandahålla eller ta
fram tjänster som är till nytta för individer och arbetsgivare. Målet med
infrastrukturen är att stärka förutsättningarna att tillhandahålla och ta fram sådana
tjänster."*. An interpretation of that is that it is not our job to develop services for *end-users*, but rather services for other *businesses* and *organization*. Among such businesses we do not count businesses who recruit labor through Arbetsförmedlingen.

- A discussion this autumn at Jobtech discussed the concept of "beställningskultur". In this context, it is unclear what product to build and *the project is exploratory in nature*. Often, software developers receive an order to build something *specific* but in the project of lifelong learning we do not have that specification.

- A point was raised that **requirement analysts** could help understand in better detail what needs to be built. They could also take on the work of communicating with the other seven agencies.

- The feedback from the organizations listed at the end of [2] could help better understand our mission.

- Also points listed above that cannot be said are part of the lifelong learning project can still be useful products from Jobtech *outside* of the lifelong learning project.

- It is a bit unclear what the meaning of "infrastructure" is.

## References

[1] Näringsdepartementet, Regeringen, 2021-06-17: *Uppdrag att utveckla en sammanhållen datainfrastruktur för kompetensförsörjning och livslångt lärande*. N2021/01915.
[2] Fredrik Ribbing, Arbetsförmedlingen, 2021-08-17: *Återrapport för livslång lärande - del 1*. Diarienummer AF-2021/0025 9083-56.
