1. Börja med `poetry new xmas-conrec`.
2. Gå till https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/conrec-utils
3. Lägg till rad, lägg till conrec-utils = {git="", rev =""}:
```
conrec-utils = { git = "https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/conrec-utils.git", rev = "b270b0344a0966c6dd7ec40ce6e9fe533fcbaa5f" }
```
4. `poetry install`.
5. `poetry shell`

```python
## Dependencies

from conrec_utils.env import default_env
import conrec_utils.taxonomy as tx

env = default_env()

## Working with the taxonomy

taxonomy = tx.load_taxonomy(env)

some_concepts = [[c["preferred_label"], c["id"]]
                 for c in taxonomy.concept_list[0:1000] if c["type"] == "occupation-name"]

concept = taxonomy.concept_map['1z8u_QoC_C56']

for k, v in concept.items():
    print(f" * {k} = {v}" )

## Dependencies

import conrec_utils.annotated_document_dataset as add

## Working with datasets

repo  = add.load_known_repository(env, "mentor-api-prod")

# repo.specs
# repo.repo_path
# repo.index_path()

# idx = repo.get_index()

## Datasets

ds = repo.full_dataset()
ds.validation_subset()
ds.train_subset()
ds.test_subset()
ds.test_subset().title()
x.key()
item = ds[0]
item.data
item.data["text"]
for x in item.data["annotations"]: print(x)

## Write an algorithm

train = ds.train_subset()

freqs = {}
ids = []
for doc in train:
    for annotation in doc.data["annotations"]:
        concept_id = annotation.get("concept-id")
        if concept_id:
            if not(concept_id in freqs):
                freqs[concept_id] = 0
            freqs[concept_id] = freqs[concept_id] + 1

print("COmputed frequencies")

most_common_concepts = list(sorted([(k, count) for k, count in freqs.items()], key=lambda x: -x[1]))[0:100]

## SHow the most common concepts
for (k, count) in most_common_concepts:
    c = taxonomy.concept_map.get(k)
    if c != None:
        print(f"{c['preferred_label']}: {count}")

## Detect concepts

import re

def detect_concept_in_text(text, concept):
    terms = [x.strip().lower()
             for x in concept["preferred_label"].split(",")]
    text = text.lower()

    def make_annotation(found):
        (start, end) = found.span()
        return {"start-position": start,
                "end-position": end,
                "matched-string": text[start:end],
                "concept-id": concept["id"],
                "type": concept["type"]}
    
    return [make_annotation(found)
            for term in terms
            for found in re.finditer(term, text)]
            
        

def detect_concepts(text):
    return {"annotations": [annotation
                            for k, count in most_common_concepts
                            for c in [taxonomy.concept_map.get(k)] if c != None
                            for annotation in detect_concept_in_text(text, c)]}

## Dependencies

import conrec_utils.annotated_document_evaluation as ade
from pathlib import Path

## Evaluate the algorithm

class XmasConrec(ade.AbstractAlgorithm):
    def process_text(self, text):
        return detect_concepts(text)

rr = ade.ResultRepository(Path("/tmp/xmas-conrec"))

ade.evaluate_algorithm(rr, [ds.validation_subset()], XmasConrec())

## Render report

context = ade.ReportingContext(env, taxonomy).with_writer(ade.MarkdownReportWriter(Path("/tmp/xmas-conrec-report"))).render_report(rr) # Leta efter 268fe9dcc7b7d5b2eb6e6102851c49a9e89d69ad

## Start a web service

server = ade.MentorApiHttpServer(XmasConrec(), taxonomy=taxonomy)
server.run_dev()

# Gå till http://localhost:5000/nlp/demo och klistra in.
```
