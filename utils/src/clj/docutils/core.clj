(ns docutils.core
  (:require [clojure.java.io :as io]
            [clojure.string :as cljstr]
            [clojure.java.shell :as shell])
  (:import [java.io File]))

(defn file-to-export? [endings ^File x]
  (let [lowname (cljstr/lower-case (.getName x))]
    (some #(cljstr/ends-with? lowname %) endings)))

(defn all-names [file]
  (loop [dst []
         file file]
    (if (nil? file)
      (-> dst rseq vec)
      (recur (conj dst (.getName file)) (.getParentFile file)))))

(defn trim-root [root-parts file-parts]
  (cond
    (empty? root-parts) file-parts
    
    (= (first root-parts) (first file-parts))
    (trim-root (rest root-parts) (rest file-parts))

    :default (throw (ex-info "Root is not a parent of file" {:root-parts root-parts
                                                             :file-parts file-parts}))))

(defn change-file-ending [filename new-ending]
  {:pre [(not= \. (first new-ending))]}
  (if-let [i (cljstr/last-index-of filename ".")]
    [(str (subs filename 0 i) "." new-ending) (subs filename (inc i))]))

(defn show-result [result]
  (when-not (zero? (:exit result))
      (println "ERROR: " result)))

(defn verbose-sh [& args]
  (let [args (map str args)]
    (println "   " (cljstr/join " " args))
    (apply shell/sh args)))

(defn pandoc-exporter [{:keys [in-file out-file in-file-ending type]}]
  (let [dst-file-str (str out-file)
        input-format (case in-file-ending
                       "md" "markdown"
                       "org" "org")]
    (io/make-parents out-file)
    (println (format "Convert '%s' -> '%s'" (str in-file) dst-file-str))
    (show-result (verbose-sh "pandoc" "-f" input-format (str in-file) "-t" type "-o" dst-file-str))))

(def pandoc {:exporter pandoc-exporter
             :in-endings #{"md" "org"}})

(defn libreoffice-exporter [{:keys [in-file out-file ending outdir]}]
  (println "Convert" (str in-file) "to" ending)
  (io/make-parents out-file)
  (show-result (verbose-sh "libreoffice" "--headless" "--convert-to" ending "--outdir" outdir (str in-file))))

(def libreoffice {:exporter libreoffice-exporter
                  :in-endings #{"odp"}})


(def formats {"latex" (merge pandoc {:ending "pdf"
                                     :type "latex"})
              "docx" (merge pandoc {:ending "docx"
                                    :type "docx"})
              "pres-pdf" (merge libreoffice {:ending "pdf"})
              "pres-pptx" (merge libreoffice {:ending "pptx"})})

(defn export-documentation [{:keys [docroot outroot fmt]}]
  {:pre [(string? docroot)
         (string? outroot)
         (string? fmt)]}
  (let [docroot (io/file docroot)
        docroot-parts (all-names docroot)
        fmt-data (get formats fmt)
        exporter (:exporter fmt-data)
        in-endings (:in-endings fmt-data)]
    (doseq [file (file-seq docroot) :when (file-to-export? in-endings file)]
      (let [parts-rel (vec (trim-root docroot-parts (all-names file)))
            n (count parts-rel)
            parents (subvec parts-rel 0 (dec n))
            old-filename (last parts-rel)
            [new-filename ending] (change-file-ending old-filename (:ending fmt-data))
            new-parts (conj parents new-filename)
            dst-file (apply io/file outroot new-parts)]
        (exporter (merge fmt-data {:in-file file
                                   :in-file-ending ending
                                   :out-file dst-file
                                   :outdir (apply io/file outroot parents)}))))))

(comment
  (do

    (export-documentation {:docroot "../" :outroot "build" :fmt "latex"})

    ))

(comment
  (do

    (export-documentation {:docroot "../" :outroot "build" :fmt "pres-pdf"})

    ))
