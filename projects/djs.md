# Publish on data.jobtechdev.se

The taxonomy has been published as json and skos files on [data.jobtechdev.se][100]. This is done by a [cron job][101] every hour.

[100]: https://data.jobtechdev.se/taxonomy/version/
[101]: https://gitlab.com/arbetsformedlingen/www/data-jobtechdev-se/djs-tax2skos
