# Concept Recognition

The objective of concept recognition is to detect taxonomy concepts in natural text. This is useful for encoding and expressing the contents of those documents using a standardized terminology of the labour market.

## Tasks

Here is a break-down of tasks being worked on.

### Collection of manually annotated documents [**ONGOING**]

We have manually annotated documents and published the annotated documents as json-files in a [git repository][100]. However, the quality of the annotations may need to be improved.

### Framework for assessing algorithms [**ONGOING**]

We are developing [`conrec-utils`][101], a Python library that makes it easy to evaluate and develop algorithms for concept recogntion. A [video][102] has been recorded to demonstrate how to use the library.

### Prototype algorithms [**NOT STARTED**]

This is the task of trying out and comparing algorithms.

## Links

| Name                                    | Description                                                |
|-----------------------------------------|------------------------------------------------------------|
| [mentor-api-prod][100]                  | Repository of annotated documents for training and testing |
| [conrec-utils][101]                     | Python library concept recognition.                        |
| [conrec-utils walkthrough (video)][102] | A video recording of how to use the conrec-utils library.  |


[100]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/annotations/mentor-api-prod
[101]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/conrec-utils
[102]: https://data.jobtechdev.se/videos/221220-taxonomy-dev-instruktionsfilm-conrec-utils-walkthrough-1920x1080.mp4

